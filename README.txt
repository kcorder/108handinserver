CISC108 Handin Server

Contents
    1. Description
    2. How to Use
    3. Directory Description
    4. Troubleshooting


README by Kevin Corder

1. Description ------------------------------------------------------

This is the customized "handin" package for Racket, which provides infrastructure
for automated handin and checking of programs from within DrRacket.
Instructors use this infrastructure to generate a separate,
course-specific ".plt" file (or package) that students install.

Install the "handin" package with:
    raco pkg install handin
or through the "Install Package..." menu item in DrRacket's "File" menu.


The repository is hosted on Bitbucket.org, making it easier to see the commit
logs and density, browse source code, read docs, and see previous and solved
problems with the "Issues" tab. Repository site can be found at:
    https://bitbucket.org/kcorder/108handinserver


2. How to Use -------------------------------------------------------

2.1. For Devs -------------------------
To create installable ".plt" file for users' submissions:

    1. Drop client folder ("handin-108" here) into
    "/Applications/Racket 6.6/collects/".

    2. In a terminal, cd into:
        "/Applications/Racket 6.6/collects/handin-108"

    3. Run:
        "raco pack --collect --at-plt --replace --all-users ++setup handin-108 handin-108.plt handin-108"

    4. Install by double-clicking the created "handin-108.plt" and
    following Dr. Racket installation instructions.

    5. Turn Dr. Racket off and on again.


    For details, see:
        "https://docs.racket-lang.org/handin-server/Client_Customization.html?q=%40test"


The server has a proxy for security reasons and has a
Certificate generation for connecting to server is done with
the openssl terminal command, and is described here:
    https://docs.racket-lang.org/handin-server/server-setup.html


Running server:
    Server can be found in UD network at 10.10.31.168, or in DNS at
    "cisc108.cis.udel.edu".

    Locally:
        Within custom server directory ("handin-server-108" here),
        run:
            racket -l handin-server
        This starts the server on whichever port is specified in the
        config.rktd found in the directory. The info.rkt in the client
        should specify the server is "localhost".

    Remotely:
        If the server is not already running (possibly if system rebooted),
        run the following command in the designated server directory:
            env DISPLAY=":10" nohup racket -l handin-server &
        This starts the server on whichever port is specified in the
        config.rktd found in the directory. The info.rkt in the client
        should specify the server is "cisc108.cis.udel.edu".

2.2. For Users ------------------------

The client-side collection can be installed by opening the .plt file and
choosing to install into DrRacket.

Students can download submissions through the handin dialog through
DrRacket, or through a web server (if "use-https" option is set for
server) at:
    https://SERVER:PORT/



3. Directory Description --------------------------------------------
Description of other files in this project:

    - directory "handin-108": the client-side package for Handin for 108
    - directory "handin-server-108d": the server-side package
    - directory "packages": contains the needed packages by various labs. The
        lab checkers should specifically mention that one of these are to be
        used.
    - directory "editedSolutions": multiple original solution files are
        incompatible with the checker when they call functions with
        overlapping standard library names, so this folder contains those
        which have been fixed. For example, in lab 1 the "string-join" has
        just been renamed "my-string-join" to avoid the collision.
    - directory "utilities": contains useful racket files for general use
    - file "README.txt": this file :)


4. Troubleshooting --------------------------------------------------
    For the connection to work, the handin-server and handin-client must both be
    findable as collections from Racket. Enter the following in Racket to get the
    path to collections (and verify they are findable):
        (collection-path "<handin-server-name>")
        (collection-path "<handin-client-name>")

    To see the collection directories, produce an error by linking a library that
    does not exist and view the error message, as:
        racket -l <non-existent library>

    See solved problems at the Issues page on repository site:
        https://bitbucket.org/kcorder/108handinserver/issues
