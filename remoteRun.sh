#!/bin/bash

## Execute this script within its containing directory
# (top level of repo) to start the X server after a reboot.
env DISPLAY=":10" nohup racket -l handin-server &

### Previous method of running server manually!
# cd 108handinserver/handin-server-108/
# nohup xvfb-run racket -l handin-server &
