#lang setup/infotab

(define name "CISC108")

(define server:port "cisc108.cis.udel.edu:7979") ; for server connection
;(define server:port "localhost:7979") ; for local connection

(define web-menu-name "Cisc108 Homepage")
(define web-address "https://sakai.udel.edu")

(define drracket-tools      `("client-gui.rkt" "tool.rkt"))
(define drracket-tool-names `(,name #f))
(define drracket-tool-icons `("icon.png" #f))

;; Auto-updater section (see handin-server/doc.txt for details)
(define enable-auto-update #t) ; enable auto-update?
(define version-filename "pl-version")
(define package-filename "pl.plt")

;; Multi-file submission section (see handin-server/doc.txt for details)
(define enable-multifile-handin #f) ; enable multi-file?
(define selection-mode 'extended) ; mode for file choose, usually 'extended
(define selection-default ; suffixes to auto-choose (a string or string-list)
  '("*.rkt" "*.rkt;*.txt"))

;; Client configuration
(define password-keep-minutes 15) ; client remembers entered password 15 mins

(define pre-install-collection  "pre-install.rkt")
;; (define post-install-collection "post-install.rkt")
