;; The first three lines of this file were inserted by DrRacket. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-beginner-reader.ss" "lang")((modname helper) (read-case-sensitive #t) (teachpacks ()) (htdp-settings #(#t constructor repeating-decimal #f #t none #f () #f)))
(require 2htdp/image)

(define (pinwheel image)
  (above (beside image (rotate -90 image))
         (beside (rotate -270 image) (rotate 180 image))))
  
(define (check-pinwheel pin-image input-image)
  (image=? pin-image (pinwheel input-image)))
           

(provide check-pinwheel)