((active-dirs ("lab1"
               "lab3"
               "lab8"
               "lab9"
               "lab10"))
 (deadline (("lab1" (2017 3 11 23 59 59) 3)
            ("lab3" (2017 3 11 23 59 59) 3)
            ("lab8" (2017 3 11 23 59 59) 3)
            ("lab9" (2017 3 11 23 59 59) 3)
            ("lab10" (2017 3 11 23 59 59) 3)
            ))
 (max-submissions (("lab1" 200)
                   ("lab3" 200)
                   ("lab8" 200)
                   ("lab9" 200)
                   ("lab10" 200)
                   ))
 (web-log-file "web-log")
 (allow-new-users #t)
; (use-https #f)
)
