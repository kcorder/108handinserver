;;Matt Saponaro
;;Checker.rkt for handin for Cisc108 Fall2016 Lab 9

;;NOTE: Need to update collection's checker.rkt if racket people didn't fix. See:
;;https://groups.google.com/forum/#!topic/racket-users/JAuH26M0mEA

(module checker handin-server/checker
  (require handin-server/grading-utils)
  (require 2htdp/image) 
  (sandbox-path-permissions
   (cons
    (list 'read "/") ;;This should be set to current directory (pwd)
    (sandbox-path-permissions)))
  (define assignment-name "Cisc108 Lab 9 Fall2016")
  ;Checks that submission is on time and that the user has submissions left
  (pre:
   (check-deadline)
   (check-max-submissions))
  ; Ends the report by adding the score and writes it in the user directory
  ; This way, students can see their reports from the web interface.
  (post:
   (add-score-to-report!)
   (write-report))
  (check:
   ; Get timestamp of the submission and add it to header and report
   :language 'racket
   :requires '(iwl-plus-image)                                                     
   :create-text? #f
   :textualize? #f
   (update-submission-timestamp!)
   (add-header-line! (get-submission-timestamp))
   (add-report-line! (get-submission-timestamp))
   (set-test-max-score! 100)
   ;;Tests for point value feedback
   (with-handlers ([exn?
                    (lambda (exn)
                      (message (format "~a will be accepted, but...\n~a"
                                       assignment-name (exn-message exn))
                               '(ok)))])

     ;;numeric?-------------------------
     ;(define-struct test (one two three))
     ;(define ADD0 (make-add 1 2))
     ;(define MUL0 (make-mul 1 2))
     ;(define ADD1 (make-add "bob" 1))
     ;(define EXP0 3)
     ;(define EXP1 'x)
     ;(define EXP2 ADD0)
     ;(define EXP3 MUL0)
     ;(define EXP4 ADD1)
     ;(define EXP5 (make-test  1 2 3))
     ;(define EXP6 (make-add (make-mul 1 2) (make-add 1 2)))
     ;(check-expect (numeric? EXP0) true)
     ;(check-expect (numeric? EXP1) false)
     ;(check-expect (numeric? EXP2) true)
     ;(check-expect (numeric? EXP3) true)
     ;(check-expect (numeric? EXP4) false)
     ;(check-expect (numeric? EXP5) false)
     ;(check-expect (numeric? EXP6) true)
     
     (!eval (define-struct test-structure (one two three)))
     (!eval (define CISC108TESTCASE-ADD0 (make-add 1 2)))
     (!eval (define CISC108TESTCASE-MUL0 (make-mul 1 2)))
     (!eval (define CISC108TESTCASE-ADD1 (make-add "bob" 1)))
     (!eval (define CISC108TESTCASE-EXP0 3))
     (!eval (define CISC108TESTCASE-EXP1 'x))
     (!eval (define CISC108TESTCASE-EXP2 CISC108TESTCASE-ADD0))
     (!eval (define CISC108TESTCASE-EXP3 CISC108TESTCASE-MUL0))
     (!eval (define CISC108TESTCASE-EXP4 CISC108TESTCASE-ADD1))
     (!eval (define CISC108TESTCASE-EXP5 (make-test-structure  1 2 3)))
     (!eval (define CISC108TESTCASE-EXP6 (make-add (make-mul 1 2) (make-add 1 2))))
     ;; numeric?: Expression --> Boolean
     ;; Consumes an expression and produces true if the Expression is a number or numeric
     ;; and false otherwise
     (@test "numeric test 1" "returned incorrect value: 3" (numeric? CISC108TESTCASE-EXP0) #t 1)
     (@test "numeric test 2" "returned incorrect value: 'x" (numeric? CISC108TESTCASE-EXP1) #f 1)
     (@test "numeric test 3" "returned incorrect value: (make-add 1 2)" (numeric? CISC108TESTCASE-EXP2) #t 1)
     (@test "numeric test 4" "returned incorrect value: (make-mul 1 2)" (numeric? CISC108TESTCASE-EXP3) #t 1)
     (@test "numeric test 5" "returned incorrect value: (make-add \"bob\" 1)" (numeric? CISC108TESTCASE-EXP4) #f 1)
     (@test "numeric test 6" "returned incorrect value: (make-test-structure  1 2 3)" (numeric? CISC108TESTCASE-EXP5) #f 1)
     (@test "numeric test 7" "returned incorrect value: (make-add (make-mul 1 2) (make-add 1 2))" (numeric? CISC108TESTCASE-EXP6) #t 1)
     
     ;;evaluate-expression-------------------------

     ;(check-expect (evaluate-expression EXP0) 3)
     ;(check-expect (evaluate-expression EXP2) 3)
     ;(check-expect (evaluate-expression EXP3) 2)
     ;(check-error (evaluate-expression2 EXP4) "input must be a numeric expression: 'grumble")
     (@test "evaluate-expression test 1" "incorrect function body" (evaluate-expression CISC108TESTCASE-EXP0) 3 1)
     (@test "evaluate-expression test 2" "incorrect function body" (evaluate-expression CISC108TESTCASE-EXP2) 3 1)
     (@test "evaluate-expression test 3" "incorrect function body" (evaluate-expression CISC108TESTCASE-EXP3) 2 1)
     (@test/exn "evaluate-expression test 4" (evaluate-expression CISC108TESTCASE-EXP4) 1)
     
     

     ;;subst-------------------------
     ;(check-expect (subst 'x 4 (make-add (make-mul 3 'x)
     ;                               (make-mul 'x 4)))
     ;         (make-add (make-mul 3 4)
     ;                   (make-mul 4 4)))
     ;(check-expect
     ; (evaluate-expression
     ;  (subst 'x 4 (make-add (make-mul 3 'x)
     ;                        (make-mul 'x 4)))) 28)
     (@test "subst test 1" "incorrect function body"
            (equal? (subst 'x 4 (make-add (make-mul 3 'x)
                                  (make-mul 'x 4)))
                    (make-add (make-mul 3 4)
                              (make-mul 4 4)))
            #t
            1)
     
     (@test "subst test 2" "incorrect function body"
            (evaluate-expression
             (subst 'x 4 (make-add (make-mul 3 'x)
                        (make-mul 'x 4))))
            28
            1)
     ;;evaluate-with-one-def-------------------------
     ;(check-expect (evaluate-with-one-def
     ;               3 (make-function-definition 'f 'x (make-add (make-add 2 4) 'x))) 3)
     ;(check-expect (evaluate-with-one-def
     ;          (make-function 'f (make-mul (make-function 'f (make-add 1 1)) 2)) 
     ;          (make-function-definition 'f 'x (make-add (make-add 2 4) 'x))) 22)
     ;(check-expect (evaluate-with-one-def
     ;               (make-function 'f (make-add 2 2)) 
     ;               (make-function-definition 'f 'x (make-add (make-add 2 4) 'x))) 10)
     ;(check-error (evaluate-with-one-def
     ;              "bob" (make-function-definition 'f 'x (make-add (make-add 2 4) 'x)))
     ;             "input must be a numeric expression: 'grumble")
     ;(check-error (evaluate-with-one-def
     ;              (make-test 1 2 3) (make-function-definition 'f 'x (make-add (make-add 2 4) 'x)))
     ;             "input must be a numeric expression: 'grumble")

     
     (@test "evaluate-with-one-def test 1" "incorrect function body"
            (evaluate-with-one-def
                    3 (make-function-definition 'f 'x (make-add (make-add 2 4) 'x)))
            3
            1)
     (@test "evaluate-with-one-def test 2" "incorrect function body"
             (evaluate-with-one-def
              (make-function 'f (make-mul (make-function 'f (make-add 1 1)) 2)) 
              (make-function-definition 'f 'x (make-add (make-add 2 4) 'x)))
             22
             1)
     (@test "evaluate-with-one-def test 3" "incorrect function body"
             (evaluate-with-one-def
                    (make-function 'f (make-add 2 2)) 
                    (make-function-definition 'f 'x (make-add (make-add 2 4) 'x)))
             10
             1)
     (@test/exn "evaluate-with-one-def test 4"
                (evaluate-with-one-def "bob" (make-function-definition 'f 'x (make-add (make-add 2 4) 'x)))
                1)
     (@test/exn "evaluate-with-one-def test 5"
                (evaluate-with-one-def
                   (make-test 1 2 3) (make-function-definition 'f 'x (make-add (make-add 2 4) 'x)))
                  1)
     ;;evaluate-with-defs-------------------------
     ;(define LOF0 (list 
     ;         (make-function-definition 'f 'x (make-add 3 'x))
     ;         (make-function-definition 'g 'x (make-mul 3 'x))
     ;         (make-function-definition 'h 'u (make-function 'f (make-mul 2 'u)))
     ;         (make-function-definition 'i 'v (make-add (make-mul 'v 'v) (make-mul 'v 'v)))
     ;         (make-function-definition 'k 'w (make-mul (make-function 'h 'w) (make-function 'i 'w)))))

     ;(define LOF1 empty)
     ;(define LOF2 (list (make-function-definition 'z 'z (make-add 'z 'z))))
     
     ;; evaluate-with-defs: Expression LOF --> Number
     ;; Consumes a expression and a LOF and produces the evaluation of the expression
     ;(check-expect (evaluate-with-defs 3 LOF0) 3)
     ;(check-expect (evaluate-with-defs (make-function 'f (make-mul (make-function 'f (make-add 1 1)) 2)) LOF0) 13)
     ;(check-expect (evaluate-with-defs (make-function 'f (make-add 2 2))LOF0) 7)
     ;(check-expect (evaluate-with-defs (make-function 'h 2) LOF0) 7)
     ;(check-expect (evaluate-with-defs (make-function 'k 2) LOF0) 56)
     ;(check-error (evaluate-with-defs (make-function 'h 2) LOF1) "input must be a numeric expression: 'grumble")
     ;(check-error (evaluate-with-defs "bob" LOF0) "input must be a numeric expression: 'grumble")
     ;(check-error (evaluate-with-defs (make-test 1 2 3) LOF0) "input must be a numeric expression: 'grumble")

     (!eval (define CISC108-TEST-LOF0 (list 
                                (make-function-definition 'f 'x (make-add 3 'x))
                                (make-function-definition 'g 'x (make-mul 3 'x))
                                (make-function-definition 'h 'u (make-function 'f (make-mul 2 'u)))
                                (make-function-definition 'i 'v (make-add (make-mul 'v 'v) (make-mul 'v 'v)))
                                (make-function-definition 'k 'w (make-mul (make-function 'h 'w) (make-function 'i 'w))))))
     
     (!eval (define CISC108-TEST-LOF1 empty))
     (!eval (define CISC108-TEST-LOF2 (list (make-function-definition 'z 'z (make-add 'z 'z)))))
     
     ;; evaluate-with-defs: Expression LOF --> Number
     ;; Consumes a expression and a LOF and produces the evaluation of the expression
     (@test "evaluate-with-defs test case 1" "incorrect function body"
            (evaluate-with-defs 3 CISC108-TEST-LOF0)
            3
            1)
     (@test "evaluate-with-defs test case 2" "incorrect function body"
            (evaluate-with-defs (make-function 'f (make-mul (make-function 'f (make-add 1 1)) 2)) CISC108-TEST-LOF0)
            13
            1)
     (@test "evaluate-with-defs test case 3" "incorrect function body"
            (evaluate-with-defs (make-function 'f (make-add 2 2)) CISC108-TEST-LOF0)
            7
            1)
     (@test "evaluate-with-defs test case 4" "incorrect function body"
            (evaluate-with-defs (make-function 'h 2) CISC108-TEST-LOF0)
            7
            1)
     (@test "evaluate-with-defs test case 5" "incorrect function body"
            (evaluate-with-defs (make-function 'k 2) CISC108-TEST-LOF0)
            56
            1)
     (@test/exn "evaluate-with-defs test case 6"
                (evaluate-with-defs (make-function 'h 2) CISC108-TEST-LOF1)
                1)
     (@test/exn "evaluate-with-defs test case 6"
                (evaluate-with-defs "bob" CISC108-TEST-LOF0)
                1)
     ;;test-structure is handin side where we created another structure. See above 
     (@test/exn "evaluate-with-defs test case 6"
                (evaluate-with-defs (make-test-structure 1 2 3) CISC108-TEST-LOF0)
                1)

     
     )))